#pragma version(1)
#pragma rs java_package_name(se.apals.cannyedge)
#pragma rs_fp_relaxed

rs_allocation gCurrentFrame;
rs_allocation gIntFrame;

// https://android.googlesource.com/platform/frameworks/rs/+/master/cpu_ref/rsCpuIntrinsicYuvToRGB.cpp
static uchar4 help(uchar y, uchar u, uchar v) {
    short Y = ((short)y) - 16;
    short U = ((short)u) - 128;
    short V = ((short)v) - 128;
    short4 p;
    p.x = (Y * 298 + V * 409 + 128) >> 8;
    p.y = (Y * 298 - U * 100 - V * 208 + 128) >> 8;
    p.z = (Y * 298 + U * 516 + 128) >> 8;
    p.w = 255;
    if(p.x < 0) {
        p.x = 0;
    }
    if(p.x > 255) {
        p.x = 255;
    }
    if(p.y < 0) {
        p.y = 0;
    }
    if(p.y > 255) {
        p.y = 255;
    }
    if(p.z < 0) {
        p.z = 0;
    }
    if(p.z > 255) {
        p.z = 255;
    }
    return (uchar4){p.x, p.y, p.z, p.w};
}

uchar4 __attribute__((kernel)) root(uint32_t x,uint32_t y)
{

    // Read in pixel values from latest frame - YUV color space
    // The functions rsGetElementAtYuv_uchar_? require API 18
    //uchar4 curPixel;
    //curPixel.r = rsGetElementAtYuv_uchar_Y(gCurrentFrame, x, y);
    //curPixel.g = rsGetElementAtYuv_uchar_U(gCurrentFrame, x, y);
    //curPixel.b = rsGetElementAtYuv_uchar_V(gCurrentFrame, x, y);
    uchar py = rsGetElementAtYuv_uchar_Y(gCurrentFrame, x, y);
    uchar pu = rsGetElementAtYuv_uchar_U(gCurrentFrame, x, y);
    uchar pv = rsGetElementAtYuv_uchar_V(gCurrentFrame, x, y);


    // uchar4 rsYuvToRGBA_uchar4(uchar y, uchar u, uchar v);
    // This function uses the NTSC formulae to convert YUV to RBG
    //uchar4 out = help(curPixel.r, curPixel.g, curPixel.b);
    return help(py, pu, pv);
    //return out;
}

