package se.apals.cannyedge;

import android.Manifest;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.SystemClock;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.renderscript.ScriptIntrinsicYuvToRGB;
import android.renderscript.Type;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class MainActivity extends Activity implements Camera.PreviewCallback, SurfaceHolder.Callback {

    private static final SparseArray<String> descriptions = new SparseArray<>();

    private static final int NUMBER_OF_MODES = 5;
    private static final int MODE_CPP = 0;
    private static final int MODE_CPP_OPENCV = 1;
    private static final int MODE_RS = 2;
    private static final int MODE_RS_INTRINSIC = 3;
    private static final int MODE_JAVA = 4;
    private static final String TAG = MainActivity.class.getSimpleName();


    static {
        System.loadLibrary("native-lib");

        descriptions.put(MODE_CPP, "CPP");
        descriptions.put(MODE_CPP_OPENCV, "CPP OPENCV");
        descriptions.put(MODE_RS, "RS");
        descriptions.put(MODE_RS_INTRINSIC, "RS INTRINSIC");
        descriptions.put(MODE_JAVA, "JAVA");
    }


    private int currentMode = 0;

    // Output bitmap, serves as a destination for the image-processing RenderScript kernel
    private Bitmap outputBitmap;

    // Single ImageView that is used to output the resulting bitmap
    private ImageView outputImageView;


    //size of processed image
    private int imageWidth;
    private int imageHeight;
    //choosen camera parameters
    private Camera.Parameters params;
    // camera in use
    private Camera camera;

    private RenderScript rs;


    // built-in RenderScript kernel for blur
    private ScriptC_process mScript;
    private ScriptIntrinsicYuvToRGB intrinsicYuvToRGB;
    private ScriptIntrinsicBlur intrinsicBlur;

    // Allocations: memory abstractions that Renderscript kernels operate on
    private Allocation allocationIn;
    private Allocation allocationBlur;
    private Allocation allocationYUV;
    private Allocation allocationOut;

    int[] rgba;
    byte[] yuv;

    final double mRadius = 5;
    Size ksize = new Size(mRadius, mRadius);
    Mat mYuv;
    Mat mRgba;
    private static final int MAX_THREADS = Runtime.getRuntime().availableProcessors();

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //get ImageView, used to display the resulting image
        outputImageView = (ImageView) findViewById(R.id.outputImageView);


        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                1337);

        camera = Camera.open(0);
        params = camera.getParameters();
        // the old movie effect does not require high resolution source
        // 640x480 resolution is enough for the effect
        // we try to choose preview resolution that is close to that
        int pixels = 1920 * 1080;
        int dMin = Math.abs(params.getPreviewSize().width * params.getPreviewSize().height - pixels);
        List<Camera.Size> sizes = params.getSupportedPreviewSizes();
        for (int i = 0; i < sizes.size(); ++i) {
            Camera.Size size = sizes.get(i);
            int d = Math.abs(size.width * size.height - pixels);
            if (d < dMin) {
                params.setPreviewSize(size.width, size.height);
                dMin = d;
            }
        }
        // we want preview in NV21 format (which is supported by all cameras, unlike RGB)
        params.setPreviewFormat(ImageFormat.NV21);
        camera.setParameters(params);

        // get real preview parameters
        params = camera.getParameters();
        if (params.getPreviewFormat() != ImageFormat.NV21) {
            Log.i("CameraRenderscript", "params.getPreviewFormat()!=ImageFormat.NV21 params.getPreviewFormat()=" + params.getPreviewFormat());
        }

        //get preview image sizes
        imageWidth = params.getPreviewSize().width;
        imageHeight = params.getPreviewSize().height;

        rgba = new int[imageWidth * imageHeight];

        camera.release();
        camera = null;


        rs = RenderScript.create(this);


        outputBitmap = Bitmap.createBitmap(imageWidth, imageHeight, Bitmap.Config.ARGB_8888);

        allocationOut = Allocation.createFromBitmap(rs, outputBitmap);
        // allocationIn and allocationBlur matches the allocationOut
        Type.Builder a = new Type.Builder(rs, Element.RGBA_8888(rs));
        a.setX(imageWidth);
        a.setY(imageHeight);
        Type rgbType = a.create();
        //Type rgbType = Type.createXY(rs, Element.RGBA_8888(rs), imageWidth, imageHeight);

        allocationIn = Allocation.createTyped(rs, rgbType, Allocation.USAGE_SCRIPT);

        Type.Builder typeYUV = new Type.Builder(rs, Element.YUV(rs));
        typeYUV.setX(imageWidth).setY(imageHeight);
        typeYUV.setYuvFormat(ImageFormat.NV21);

        // allocation for the YUV input from the camera
        allocationYUV = Allocation.createTyped(rs, typeYUV.create(), Allocation.USAGE_SCRIPT);
        allocationBlur = Allocation.createTyped(rs, allocationOut.getType(), Allocation.USAGE_SCRIPT);

        intrinsicYuvToRGB = ScriptIntrinsicYuvToRGB.create(rs, Element.U8_4(rs));
        intrinsicYuvToRGB.setInput(allocationYUV);
        intrinsicBlur = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        mScript = new ScriptC_process(rs);
        mScript.set_gCurrentFrame(allocationYUV);

        if (!OpenCVLoader.initDebug()) {
            Log.e(MainActivity.class.getSimpleName(), "WHAT THe Shit");
        } else {
            Log.e(MainActivity.class.getSimpleName(), "YEeEeEE");
        }


        SurfaceView surView = (SurfaceView) findViewById(R.id.inputSurfaceView);
        SurfaceHolder surHolder = surView.getHolder();
        surHolder.addCallback(this);
    }

    int[] pixelBuf;

    private void decodeYUV420SP(
            int[] out,
            int sz,
            int width,
            int height,
            int j,
            int i,
            int Y,
            int Cr,
            int Cb,
            int pixPtr,
            int jDiv2,
            int R,
            int G,
            int B,
            int cOff,
            int w,
            int h) {


        for (; j < h; j++) {
            pixPtr = j * w;
            jDiv2 = j >> 1;
            for (i = 0; i < w; i++) {
                Y = yuv[pixPtr];
                if (Y < 0) Y += 255;
                if ((i & 0x1) != 1) {
                    cOff = sz + jDiv2 * w + (i >> 1) * 2;
                    Cb = yuv[cOff];
                    if (Cb < 0) Cb += 127;
                    else Cb -= 128;
                    Cr = yuv[cOff + 1];
                    if (Cr < 0) Cr += 127;
                    else Cr -= 128;
                }
                R = Y + Cr + (Cr >> 2) + (Cr >> 3) + (Cr >> 5);
                if (R < 0) R = 0;
                else if (R > 255) R = 255;

                G = Y - (Cb >> 2) + (Cb >> 4) + (Cb >> 5) - (Cr >> 1) + (Cr >> 3) + (Cr >> 4) +
                        (Cr >> 5);
                if (G < 0) G = 0;
                else if (G > 255) G = 255;


                B = Y + Cb + (Cb >> 1) + (Cb >> 2) + (Cb >> 6);
                if (B < 0) B = 0;
                else if (B > 255) B = 255;

                out[pixPtr++] = 0xff000000 + (B << 16) + (G << 8) + R;
            }
        }

    }


    byte[] forjava;
    byte[] forcpp;
    byte[] forcppopencv;
    byte[] forrs;
    byte[] forrsinstrinsic;
    int[] outjava;
    int[] outcpp;
    int[] outcppopencv;
    int[] outrs;
    int[] outrsintrinsic;
    int countedFrames = 0;

    long start = 0;
    long end = 0;
    long javatime, cpptime, rstime, cppopencvtime, rsintrinsictime;
    long javatimemin = Long.MAX_VALUE,
            cpptimemin = Long.MAX_VALUE,
            rstimemin = Long.MAX_VALUE,
            cppopencvtimemin = Long.MAX_VALUE,
            rsintrinsictimemin = Long.MAX_VALUE;
    long javatimemax, cpptimemax, rstimemax, cppopencvtimemax, rsintrinsictimemax;

    ArrayList<Long> javatimes = new ArrayList<>(100);
    ArrayList<Long> cpptimes = new ArrayList<>(100);
    ArrayList<Long> opencvtimes = new ArrayList<>(100);
    ArrayList<Long> rstimes = new ArrayList<>(100);
    ArrayList<Long> intrinsictimes = new ArrayList<>(100);

    @Override
    public void onPreviewFrame(byte[] arg0, Camera arg1) {
        if (mYuv == null) {
            mYuv = new Mat(imageHeight + imageHeight / 2, imageWidth, CvType.CV_8UC1);
            mRgba = new Mat();
            pixelBuf = new int[arg0.length];
            yuv = new byte[arg0.length];
            forjava = new byte[arg0.length];
            forcpp = new byte[arg0.length];
            forcppopencv = new byte[arg0.length];
            forrs = new byte[arg0.length];
            forrsinstrinsic = new byte[arg0.length];

            outjava = new int[imageWidth * imageHeight];
            outcpp = new int[imageWidth * imageHeight];
            outcppopencv = new int[imageWidth * imageHeight];
            outrs = new int[imageWidth * imageHeight];
            outrsintrinsic = new int[imageWidth * imageHeight];
        }
        System.arraycopy(arg0, 0, yuv, 0, arg0.length);
        System.arraycopy(arg0, 0, forjava, 0, arg0.length);
        System.arraycopy(arg0, 0, forcpp, 0, arg0.length);
        System.arraycopy(arg0, 0, forcppopencv, 0, arg0.length);
        System.arraycopy(arg0, 0, forrs, 0, arg0.length);
        System.arraycopy(arg0, 0, forrsinstrinsic, 0, arg0.length);
        Log.e(MainActivity.class.getSimpleName(), "yee boii: " + countedFrames);

        if (countedFrames > 50) {
            timeJava();
            timeCpp();
            timeCppOpencv();
            timeRs();
            timeRsIntrinsic();
//            compare(outcpp, outcppopencv, outjava, outrs, outrsintrinsic);
        }

        if (countedFrames > 149) {
            long avgjava = TimeUnit.NANOSECONDS.toMillis(javatime / 100);
            printList("Java", javatimes);
            long avgcpp = TimeUnit.NANOSECONDS.toMillis(cpptime / 100);
            printList("CPP", cpptimes);
            long avgcppopencv = TimeUnit.NANOSECONDS.toMillis(cppopencvtime / 100);
            printList("OPENCV", opencvtimes);
            long avgrs = TimeUnit.NANOSECONDS.toMillis(rstime / 100);
            printList("RS", rstimes);
            long avgrsintrinsic = TimeUnit.NANOSECONDS.toMillis(rsintrinsictime / 100);
            printList("RS Intrinsic", intrinsictimes);
            Log.e(TAG, avgjava + " ms for java");
            Log.e(TAG, avgcpp + " ms for cpp");
            Log.e(TAG, avgcppopencv + " ms for cpp opencv");
            Log.e(TAG, avgrs + " ms for rs");
            Log.e(TAG, avgrsintrinsic + " ms for rs intrinscis");
            Camera.Size s = camera.getParameters().getPreviewSize();
            Log.e(TAG, "height: " + s.height + ", width: " + s.width);
            camera.setPreviewCallback(null);
        }
//
//        switch (currentMode) {
//            case MODE_CPP:
//                cpp(arg0);
//                break;
//            case MODE_CPP_OPENCV:
//                cpp_opencv(arg0);
//                break;
//            case MODE_JAVA:
//                java(arg0);
//                break;
//            case MODE_RS:
//                rs(arg0);
//                break;
//            case MODE_RS_INTRINSIC:
//                rsIntrinsic(arg0);
//                break;
//            default:
//                throw new RuntimeException("what have you done");
//        }

//        outputImageView.setImageBitmap(outputBitmap);
//        outputImageView.invalidate();
        countedFrames++;
    }

    private void printList(String name, ArrayList<Long> javatimes) {
        String str = "[";
        for (int i = 0; i < javatimes.size(); i++) {
            str += TimeUnit.NANOSECONDS.toMillis(javatimes.get(i)) + ", ";
        }
        str += "]";
        Log.d(TAG, name + ": " + str);
    }


    private void timeRsIntrinsic() {
        start = SystemClock.elapsedRealtimeNanos();

        rsIntrinsic(forrsinstrinsic, outrsintrinsic);
        end = SystemClock.elapsedRealtimeNanos();
        long time = end - start;
        intrinsictimes.add(time);
        rsintrinsictime += time;
        if (time > rsintrinsictimemax) {
            rsintrinsictimemax = time;
        } else if (time < rsintrinsictimemin) {
            rsintrinsictimemin = time;
        }
    }


    private void timeRs() {
        start = SystemClock.elapsedRealtimeNanos();

        rs(forrs, outrs);
        end = SystemClock.elapsedRealtimeNanos();
        long time = end - start;
        rstimes.add(time);
        rstime += time;
        if (time > rstimemax) {
            rstimemax = time;
        } else if (time < rstimemin) {
            rstimemin = time;
        }
    }


    private void timeCppOpencv() {
        start = SystemClock.elapsedRealtimeNanos();

        cpp_opencv(forcppopencv, outcppopencv);
        end = SystemClock.elapsedRealtimeNanos();
        long time = end - start;
        opencvtimes.add(time);
        cppopencvtime += time;
        if (time > cppopencvtimemax) {
            cppopencvtimemax = time;
        } else if (time < cpptimemin) {
            cppopencvtimemin = time;
        }
    }


    private void timeCpp() {
        start = SystemClock.elapsedRealtimeNanos();

        cpp(forcpp, outcpp);
        end = SystemClock.elapsedRealtimeNanos();
        long time = end - start;
        cpptimes.add(time);
        cpptime += time;
        if (time > cpptimemax) {
            cpptimemax = time;
        } else if (time < cpptimemin) {
            cpptimemin = time;
        }
    }

    private void timeJava() {
        start = SystemClock.elapsedRealtimeNanos();
        java(forjava, outjava);
        end = SystemClock.elapsedRealtimeNanos();
        long time = end - start;
        javatimes.add(time);
        javatime += time;
        if (time > javatimemax) {
            javatimemax = time;
        } else if (time < javatimemin) {
            javatimemin = time;
        }
    }

    private void compare(int[] outcpp, int[] outcppopencv, int[] outjava, int[] outrs, int[] outrsintrinsic) {
        // 0 = outcpp
        // 1 = opencv
        // 2 = java
        // 3 = rs
        // 4 = rs intrinsic
        // 4 == 3, 2 == 0
        int[][] diffs = new int[5][5];

        int[][] a = new int[][]{outcpp, outcppopencv, outjava, outrs, outrsintrinsic};
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < i; j++) {
                if (Arrays.equals(a[i], a[j])) {
                    Log.d(TAG, i + " and " + j + " is identical!!!!");
                } else {

                    for (int k = 0; k < a.length; k++) {
                        for (int l = 0; l < a.length && l != k; l++) {
                            int[] first = a[k];
                            int[] second = a[l];
                            for (int m = 0; m < first.length; m++) {
                                int firstPixel = first[m];
                                int r1 = (firstPixel >> 16) & 0xFF;
                                int g1 = (firstPixel >> 8) & 0xFF;
                                int b1 = (firstPixel >> 0) & 0xFF;

                                int secondPixel = second[m];
                                int r2 = (secondPixel >> 16) & 0xFF;
                                int g2 = (secondPixel >> 8) & 0xFF;
                                int b2 = (secondPixel >> 0) & 0xFF;

                                int diff = Math.abs(r2 - r1) + Math.abs(g2 - g1) + Math.abs(b2 - b1);
                                diffs[k][l] = diff;
                            }
                        }

                    }
                }
            }
        }

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < i; j++) {
                Log.d(TAG, "Diffs, " + i + " : " + j + ": " + diffs[i][j]);
            }
        }
    }

    private void java(final byte[] arg0, final int[] out) {

        Thread[] threads = new Thread[MAX_THREADS];
        for (int i = 0; i < MAX_THREADS; i++) {
            final int finalI = i;
            Thread a = new Thread(new Runnable(
            ) {
                @Override
                public void run() {
                    int start = imageHeight * finalI / MAX_THREADS;
                    int end = (finalI + 1) * imageHeight / MAX_THREADS;
                    decodeYUV420SP(
                            out,
                            imageWidth * imageHeight,
                            imageWidth,
                            imageHeight,
                            start,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            imageWidth,
                            end
                    );
                }
            });
            a.start();
            threads[i] = a;
        }

        for (int i = 0; i < MAX_THREADS; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e("MainActivity", "ye boiiii");
            }
        }

        outputBitmap.setPixels(rgba, 0, imageWidth, 0, 0, imageWidth, imageHeight);
//        outputImageView.setImageBitmap(applyGaussianBlur(outputBitmap));
    }

    private void rsIntrinsic(byte[] arg0, int[] out) {
        allocationYUV.copyFrom(arg0);
        intrinsicYuvToRGB.forEach(allocationOut);

        // Calculations
        // https://android.googlesource.com/platform/cts/+/master/tests/tests/renderscript/src/android/renderscript/cts/intrinsic_blur.rs
//        intrinsicBlur.setInput(allocationIn);
//        intrinsicBlur.forEach(allocationOut);

        allocationOut.syncAll(Allocation.USAGE_SHARED);
        outputBitmap.getPixels(out, 0, imageWidth, 0, 0, imageWidth, imageHeight);
    }

    private void rs(byte[] arg0, int[] out) {
        allocationYUV.copyFrom(arg0);
        mScript.forEach_root(allocationOut);


        // Calculations
        // https://android.googlesource.com/platform/cts/+/master/tests/tests/renderscript/src/android/renderscript/cts/intrinsic_blur.rs
//        intrinsicBlur.setInput(allocationIn);
//        intrinsicBlur.forEach(allocationOut);

        allocationOut.syncAll(Allocation.USAGE_SHARED);

        outputBitmap.getPixels(out, 0, imageWidth, 0, 0, imageWidth, imageHeight);
    }

    private void cpp_opencv(byte[] arg0, int[] out) {
        mYuv.put(0, 0, arg0);
        Imgproc.cvtColor(mYuv, mRgba, Imgproc.COLOR_YUV2BGR_NV12);
        Utils.matToBitmap(mRgba, outputBitmap);
        outputBitmap.getPixels(out, 0, imageWidth, 0, 0, imageWidth, imageHeight);
    }

    private void cpp(final byte[] arg0, final int[] out) {
        toRgb(out, arg0, imageWidth, imageHeight);
        outputBitmap.setPixels(pixelBuf, 0, imageWidth, 0, 0, imageWidth, imageHeight);
    }


    native void toRgb(int[] rgb, byte[] yuv, int width, int height);


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            camera = Camera.open(0);
            camera.setParameters(params);
            camera.setPreviewCallback(this);
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        camera.stopPreview();
        camera.setPreviewCallback(null);
        camera.release();
        camera = null;
    }

    public void toggleMode(View view) {
        currentMode = (++currentMode) % NUMBER_OF_MODES;
    }
}
