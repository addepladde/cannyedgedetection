#include <jni.h>
#include <string>
#include <opencv2/core/core.hpp>
#include <android/log.h>


struct arg_struct {
    int sz;
    int i;
    int j;
    int Y;
    int Cr;
    int Cb;
    int pixPtr;
    int jDiv2;
    int R;
    int G;
    int B;
    int cOff;
    int w;
    int h;
};

jbyte *yuv;

extern "C"
{

void *convert(void *arguments);



int *rgbData;
int rgbDataSize = 0;


JNIEXPORT void JNICALL Java_se_apals_cannyedge_MainActivity_toRgb(JNIEnv *env, jobject obj,
                                                                  jintArray rgb,
                                                                  jbyteArray yuv420sp,
                                                                  jint width,
                                                                  jint height) {
    int sz;
//    int i = 0;
//    int j;
    int Y;
    int Cr = 0;
    int Cb = 0;
    int pixPtr = 0;
    int jDiv2 = 0;
    int R = 0;
    int G = 0;
    int B = 0;
    int cOff;
    int w = width;
    int h = height;
    sz = w * h;
    int tmp[sz];

    yuv = env->GetByteArrayElements(yuv420sp, NULL);
    if (rgbDataSize < sz) {
        rgbData = &tmp[0];
        rgbDataSize = sz;
        __android_log_write(ANDROID_LOG_INFO, "JNI", "alloc");
    }

    int a = 0;
    int MAX_THREADS = 4;
    pthread_t threads[MAX_THREADS];
    struct arg_struct args[MAX_THREADS];

    for (; a < MAX_THREADS; a++) {
        pthread_t convert_thread;
        args[a].sz = sz;
        args[a].i = 0;
        args[a].j = h * a / MAX_THREADS;
        args[a].Y = 0;
        args[a].Cr = Cr;
        args[a].Cb = Cb;
        args[a].pixPtr = pixPtr;
        args[a].jDiv2 = jDiv2;
        args[a].R = R;
        args[a].G = G;
        args[a].B = B;
        args[a].cOff = 0;
        args[a].w = w;
        args[a].h = (a + 1) * h / MAX_THREADS;

//
        if (pthread_create(&convert_thread, NULL, convert, &args[a])) {
            fprintf(stderr, "Error creating thread\n");
        }
        threads[a] = convert_thread;
    }

    for (a = 0; a < MAX_THREADS; a++) {
        pthread_join(threads[a], NULL);
    }


    env->SetIntArrayRegion(rgb, 0, sz, &rgbData[0]);
}

}

void *convert(void *arguments) {
    struct arg_struct *args = (struct arg_struct *) arguments;

//    __android_log_write(ANDROID_LOG_DEBUG, "JNI", "i am in the func I REPEAT I the func");

    int sz = args->sz;
    int i;
    int j = args->j;
    int Y;
    int Cr = args->Cr;
    int Cb = args->Cb;
    int pixPtr;
    int jDiv2;
    int R;
    int G;
    int B;
    int cOff;
    int w = args->w;
    int h = args->h;

    for (; j < h; j++) {
        pixPtr = j * w;
        jDiv2 = j >> 1;
        for (i = 0; i < w; i++) {
            Y = yuv[pixPtr];
            if (Y < 0) Y += 255;
            if ((i & 0x1) != 1) {
                cOff = sz + jDiv2 * w + (i >> 1) * 2;
                Cb = yuv[cOff];
                if (Cb < 0) Cb += 127; else Cb -= 128;
                Cr = yuv[cOff + 1];
                if (Cr < 0) Cr += 127; else Cr -= 128;
            }
            R = Y + Cr + (Cr >> 2) + (Cr >> 3) + (Cr >> 5);
            if (R < 0) R = 0;
            else if (R > 255) R = 255;
            G = Y - (Cb >> 2) + (Cb >> 4) + (Cb >> 5) - (Cr >> 1) + (Cr >> 3) + (Cr >> 4) +
                (Cr >> 5);
            if (G < 0) G = 0;
            else if (G > 255) G = 255;
            B = Y + Cb + (Cb >> 1) + (Cb >> 2) + (Cb >> 6);
            if (B < 0) B = 0;
            else if (B > 255) B = 255;

            rgbData[pixPtr++] = 0xff000000 + (B << 16) + (G << 8) + R;
        }
    }

    return NULL;
}
//
//short Y = ((short)y) - 16;
//short U = ((short)u) - 128;
//short V = ((short)v) - 128;
//short4 p;
//p.x = (Y * 298 + V * 409 + 128) >> 8;
//p.y = (Y * 298 - U * 100 - V * 208 + 128) >> 8;
//p.z = (Y * 298 + U * 516 + 128) >> 8;
//p.w = 255;
//if(p.x < 0) {
//p.x = 0;
//}
//if(p.x > 255) {
//p.x = 255;
//}
//if(p.y < 0) {
//p.y = 0;
//}
//if(p.y > 255) {
//p.y = 255;
//}
//if(p.z < 0) {
//p.z = 0;
//}
//if(p.z > 255) {
//p.z = 255;
//}
//return (uchar4){p.x, p.y, p.z, p.w};
